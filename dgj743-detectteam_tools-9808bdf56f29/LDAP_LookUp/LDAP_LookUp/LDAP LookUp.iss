; -- Example2.iss --
; Same as Example1.iss, but creates its icon in the Programs folder of the
; Start Menu instead of in a subfolder, and also creates a desktop icon.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=LDAP LookUp
AppVersion=2.01
WizardStyle=modern
DefaultDirName={autopf}\LDAP LookUp
; Since no icons will be created in "{group}", we don't need the wizard
; to ask for a Start Menu folder name:
DisableProgramGroupPage=yes
UninstallDisplayIcon={app}\LDAP_LookUp.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Inno Setup Examples Output


[Files]
Source: "LDAP_LookUp.exe"; DestDir: "{app}"
Source: "ad_common_tools.py"; DestDir: "{app}"
Source: "ad_config.json"; DestDir: "{app}"
Source: "_asyncio.pyd"; DestDir: "{app}"
Source: "_bz2.pyd"; DestDir: "{app}"
Source: "_contextvars.pyd"; DestDir: "{app}"
Source: "_ctypes.pyd"; DestDir: "{app}"
Source: "_decimal.pyd"; DestDir: "{app}"
Source: "_elementtree.pyd"; DestDir: "{app}"
Source: "_hashlib.pyd"; DestDir: "{app}"
Source: "_lzma.pyd"; DestDir: "{app}"
Source: "_msi.pyd"; DestDir: "{app}"
Source: "_multiprocessing.pyd"; DestDir: "{app}"
Source: "_overlapped.pyd"; DestDir: "{app}"
Source: "_queue.pyd"; DestDir: "{app}"
Source: "_socket.pyd"; DestDir: "{app}"
Source: "_sqlite3.pyd"; DestDir: "{app}"
Source: "_ssl.pyd"; DestDir: "{app}"
Source: "_tkinter.pyd"; DestDir: "{app}"
Source: "api-ms-win-core-console-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-datetime-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-debug-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-errorhandling-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-file-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-file-l1-2-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-file-l2-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-handle-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-heap-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-interlocked-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-libraryloader-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-localization-l1-2-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-memory-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-namedpipe-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-processenvironment-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-processthreads-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-processthreads-l1-1-1.dll"; DestDir: "{app}"
Source: "api-ms-win-core-profile-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-rtlsupport-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-string-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-synch-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-synch-l1-2-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-sysinfo-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-timezone-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-core-util-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-conio-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-convert-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-environment-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-filesystem-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-heap-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-locale-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-math-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-process-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-runtime-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-stdio-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-string-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-time-l1-1-0.dll"; DestDir: "{app}"
Source: "api-ms-win-crt-utility-l1-1-0.dll"; DestDir: "{app}"
Source: "comctl32.dll"; DestDir: "{app}"
Source: "libcrypto-1_1-x64.dll"; DestDir: "{app}"
Source: "libssl-1_1-x64.dll"; DestDir: "{app}"
Source: "Lookup.ico"; DestDir: "{app}"
Source: "pyexpat.pyd"; DestDir: "{app}"
Source: "python37.dll"; DestDir: "{app}"
Source: "select.pyd"; DestDir: "{app}"
Source: "sqlite3.dll"; DestDir: "{app}"
Source: "tcl86t.dll"; DestDir: "{app}"
Source: "tk86t.dll"; DestDir: "{app}"
Source: "ucrtbase.dll"; DestDir: "{app}"
Source: "unicodedata.pyd"; DestDir: "{app}"
Source: "vcruntime140.dll"; DestDir: "{app}"
Source: "Readme.txt"; DestDir: "{app}"; Flags: isreadme

[Icons]
Name: "{autoprograms}\LDAP LookUp"; Filename: "{app}\LDAP_LookUp.exe"; WorkingDir: "{app}"
Name: "{autodesktop}\LDAP LookUp"; Filename: "{app}\LDAP_LookUp.exe"
