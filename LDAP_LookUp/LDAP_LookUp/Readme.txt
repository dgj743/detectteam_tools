Thank you for using this simple LDAP Lookup script.

Download the LDAP_Lookup directory and its contents, or the LDAPLookUp.zip file.

When download is complete, either unzip the zipped file and then navigate into the LDAP_LookUp directory,
or navigate directly into the LDAP_LookUp directory you just downloaded.

Run the program: "mysetup.exe"

Follow all the prompts and an Icon will be placed on your desktop with the name of LDAP LookUp.

Double click the icon and the script will run.


=====      Below are      =====
===== Prompt Explinations =====

"Please Enter your Core ID:"  
"Password:"   

These two questions are to get your credentials, so that the script can authenticate to LDAP.
Note:  The Password response should be masked by atrics.



"Enter the object name of the item you are looking for, or the word exit to leave the program. example: dxa916 or glo-sec-grp:"
This question is to obtain the common name of the object that you are looking for.



"Please enter what type of search this is. ie: coreid, group, workstation, common, or exit.:"
This is to make sure the most accurate response from LDAP is displayed, or to exit the script.